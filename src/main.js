import Vue from 'vue';
// import VueRouter from 'vue-router';

import App from './App.vue';
//import Service from './Service.vue';
//import Team from './Team.vue';
//import Shop from './Shop.vue';

// Vue.use(VueRouter);

// const Home = {
//   template: Vue.component('app', App)
// };

// const ServicePage = {
//   template: Vue.component('service', Service)
// };

// const  TeamPage = {
//   template: Vue.component('team', Team)
// };

// const ShopPage = {
//   template: Vue.component('shop', Shop)
// };

// const routes = [
//   { path: '/', component: Home },
//   { path: '/service', component: Service },
//   { path: '/team', component: Team },
//   { path: '/shop', component: Shop },
// ];

// const router = new VueRouter({
//   routes // short for `routes: routes`
// })

new Vue({
  el: '#app',
  render: h => h(App),
})

